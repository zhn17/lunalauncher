package net.minecraft.launcher.updater;

import com.mojang.launcher.Http;
import com.mojang.launcher.OperatingSystem;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;

public class RemoteVersionList extends VersionList {

    private final String baseUrl;
    private final Proxy proxy;

    public RemoteVersionList(final String baseUrl, final Proxy proxy) {
        this.baseUrl = baseUrl;
        this.proxy = proxy;
    }

    @Override
    public String getContent(final String path) throws IOException {
        return Http.performGet(this.getUrl(path), this.proxy);
    }

    @Override
    public boolean hasAllFiles(final CompleteMinecraftVersion version, final OperatingSystem os) {
        return true;
    }

    @Override
    public URL getUrl(final String file) throws MalformedURLException {
        return new URL(this.baseUrl + file);
    }

    public Proxy getProxy() {
        return this.proxy;
    }
}
