package com.mojang.launcher.updater;

import com.mojang.launcher.events.RefreshedVersionsListener;
import com.mojang.launcher.updater.download.DownloadJob;
import com.mojang.launcher.versions.CompleteVersion;
import com.mojang.launcher.versions.ReleaseType;
import com.mojang.launcher.versions.Version;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

public interface VersionManager {

    void refreshVersions() throws IOException;

    List<VersionSyncInfo> getVersions();

    List<VersionSyncInfo> getVersions(VersionFilter<? extends ReleaseType> p0);

    VersionSyncInfo getVersionSyncInfo(Version p0);

    VersionSyncInfo getVersionSyncInfo(String p0);

    VersionSyncInfo getVersionSyncInfo(Version p0, Version p1);

    List<VersionSyncInfo> getInstalledVersions();

    CompleteVersion getLatestCompleteVersion(VersionSyncInfo p0) throws IOException;

    DownloadJob downloadVersion(VersionSyncInfo p0, DownloadJob p1) throws IOException;

    DownloadJob downloadResources(DownloadJob p0, CompleteVersion p1) throws IOException;

    ThreadPoolExecutor getExecutorService();

    void addRefreshedVersionsListener(RefreshedVersionsListener p0);

    void removeRefreshedVersionsListener(RefreshedVersionsListener p0);

    VersionSyncInfo syncVersion(VersionSyncInfo p0) throws IOException;

    void installVersion(CompleteVersion p0) throws IOException;

    void uninstallVersion(CompleteVersion p0) throws IOException;
}
